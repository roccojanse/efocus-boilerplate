@echo off
echo.
echo eFocus Frontend Boilerplate setup
echo =================================
echo by Gideon Heilbron en Rocco Janse
echo.

if "%1"=="" goto error
if "%2"=="" goto update
mkdir %2
cd %2

:update
echo.
echo Fetching latest version...
echo.
git clone https://roccojanse:w8w00rd@bitbucket.org/roccojanse/efocus-boilerplate.git

:copyfiles
echo.
echo Copying boilerplate files
echo =========================
call xcopy efocus-boilerplate\*.* /E /Y /Q .

:update_vars
echo.
echo Setting project name to "%1"
echo ============================

setlocal enabledelayedexpansion
set CONFIGJSON=package-config.json
set CONFIGOUTPUT=package-config_temp.json
set SEARCHTEXT=[[eFocusBoilerplate]]
set REPLACETEXT=%1

for /f "tokens=1,* delims=¶" %%A in ('"type %cd%\%CONFIGJSON%"') do (
    set string=%%A
    set modified=!string:%SEARCHTEXT%=%REPLACETEXT%!

    echo !modified! >> %cd%\%CONFIGOUTPUT%
)
del %cd%\%CONFIGJSON%
rename %cd%\%CONFIGOUTPUT% %CONFIGJSON%

echo Done.

:install
echo.
echo Installing default dependencies
echo ===============================
echo This may take a while...
call npm install --silent
echo.
echo.
echo.
echo ...almost done.
call bower install --silent

:init
echo.
echo Initialise
echo ==========
call grunt init

:cleanup
echo.
echo Cleanup
echo =======
echo.
rd /s /q efocus-boilerplate

:startup
echo.
echo Opening browser window...
start "" "file:///%cd%/dist/index.html"

:end
echo.
echo Done.
goto:eof

:error
echo.
echo ERROR: You need to specify a project name!
echo usage: setup [projectname] [newfolder (optional)]
echo.