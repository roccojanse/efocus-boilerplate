/**
 * @name App
 * @version 0.0.2 [Januari 2015]
 * @author Rocco Janse <rocco.janse@efocus.nl>
 * @author Gideon Heilbron <gideon.heilbron@efocus.nl>
 * @copyright Copyright 2015 eFocus, www.efocus.nl
 * @fileOverview Application
 */

var App = App || {};

(function(App, window, document) {

    App.name = 'Application';

    console.log($, App);

})(App, window, document);