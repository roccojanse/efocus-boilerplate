# eFocus Frontend Boilerplate documentation

The eFocus Frontend Boilerplate (EFB) contains all needed dependencies, libraries, files and resources to start a default project. EFB is easy ton install and insures a consistent workspace of most projects. EFB consists of Bower, Grunt, jQuery among others.

###### version 0.1.0 - by Gideon Heilbron and Rocco Janse, 2014/2015
###### (c)eFocus - http://www.efocus.nl

### Summary
  - Clone boilerplate via GIT
  - Install Ruby and Nodejs (if necessary)
  - Install dependencies via Bower and Node Package Manager (npm)
  - Run build process for the first time
  - Profit!

### Starting project

First clone the eFocus Boilerplate Git repository. Open the command prompt (win) or your terminal (mac), navigate to or create a temp directory and type:

```sh
$ git clone https://yourbitbucketusername@bitbucket.org/roccojanse/efocus-boilerplate.git
```
Copy the files in your temp directory to your workspace in your project. Your basic project setup and architecture is now ready.

### Installation
Please be sure that you have:

Nodejs (http://nodejs.org/download/) + Ruby(http://rubyinstaller.org/) installed.

After installing and rebooting run the following command in your console: "gem install sass". This will make sure the Sass Grunt task will run without problems. If the console gives an error, try to sudo the install command.


### Installing project dependencies
The external libraries (such as jQuery and/or Modernizr) are installed via Bower. The dependencies needed for our buildprocess are installed via Node Package Manager (npm). To install all project dependencies type the following commands in the command prompt or terminal. Make sure you are in the root of your workspace (the folder your project lives in and where the Gruntfile.js + package.json are located).

```sh
$ npm install
$ bower install
```

### Running the initial build process
To run the initial build process type the following command in your command prompt or terminal. Again, make sure you are in your project root directory.

```sh
$ grunt init
```

### Running the default build process
To run the default build process type the following command in your command prompt or terminal.

```sh
$ grunt
```

### Watching changes and build automatically
To run the build process automatically if files change, type the following command in your command prompt or terminal.

```sh
$ grunt watch
```

### Getting started
  - setup <projectname> <path [optional]>