module.exports = function(grunt){
	'use strict';

	// All scss files
	var srcScssFiles = ['src/app.scss', 'src/scss/*.scss', 'src/scss/**/*.scss']
		,	srcJsFiles = ['src/app/**/*.js', 'src/app.js'];

	// Load time of Grunt does not slow down even if there are many plugins
	require('jit-grunt')(grunt);

	// Measures the time each task takes
	require('time-grunt')(grunt);

	grunt.initConfig({

		// get site config like package name and application paths etc
		config: grunt.file.readJSON('config.json'),


		// Notify task - Notify user when Grunt finishes a task.
		// Docs: https://github.com/dylang/grunt-notify
		notify: {
			watch: {
				options: {
					title: 'Watch processed',
					message: 'Grunt noticed a filechange'
				}
			},
			build: {
				options: {
					title: 'Build',
					message: 'Grunt build and compiled scss'
				}
			},
			assets: {
				options: {
					title: 'Assets',
					message: 'Build scss and concatenated all js files'
				}
			},
			production: {
				options: {
					title: 'Production',
					message: 'Everything build, concatenated and minified/uglified'
				}
			}
		},


		// Sass task - Process SASS/SCSS to CSS.
		// Docs: https://github.com/gruntjs/grunt-contrib-sass
		sass: {
			app: {
				options: {
					style: 'expanded',
					sourcemap: 'auto',
					trace: true
				},
				files: {
					//   'destination': 'source'
					'dist/css/app.css': 'src/app.scss',
          'dist/css/modules.css': 'src/components/**/*.scss'
				}
			}
		},


		// JSHint task - Check JS files and display errors in console.
		// Docs: https://github.com/gruntjs/grunt-contrib-jshint
		// Options: http://www.jshint.com/docs/
		jshint: {
			files: srcJsFiles,
			options: {
				undef: true,
				unused: false,
				loopfunc: true,
				browser: true,
				devel: true,
				jquery: true,
				globals: {
					console: true
				}
			}
		},


		// Watch task - Run tasks whenever watched files change.
		// Docs: https://github.com/gruntjs/grunt-contrib-watch
		watch: {
			js: {
				files: ['src/app.js'],
				tasks: ['update-javascript']
			},
			sass: {
				files: srcScssFiles,
				tasks: ['sass:app', 'autoprefixer', 'notify:watch']
			},
			css: {
				files: ['*.css']
			},
			livereload: {
				files: ['dist/css/*.css', 'dist/js/*.js'],
				options: {
					livereload: true
				}
			}
		},


		// Bower concatenate task - Concatenate Bower dependencies.
		// Docs: https://github.com/sapegin/grunt-bower-concat
		bower_concat: {
			all: {
				dependencies: {
					'modernizr': 'jquery',
					'jquery.cookie': 'jquery'
				},
				dest: 'src/libs/libs.js'
			}
		},


		// Clean task - Clear files and folders.
		// Docs: https://github.com/gruntjs/grunt-contrib-clean
		clean: {
			fonts: {
				src: ['dist/assets/fonts/**/*.{eot,woff,ttf,svg}']
			},
			assets: {
				src: ['dist/assets/img/**/*.{png,jpg,gif}', 'dist/assets/svg/**/*.{svg}']
			}
		},


		// Copy task - Copies files and folders
		// Docs: https://github.com/gruntjs/grunt-contrib-copy
		copy: {
			ico: {
				files: [{
					expand: true,
					cwd: 'src',
					src: ['assets/*.ico'],
					dest: 'dist'
				}]
			},
			fonts: {
				files: [{
					expand: true,
					cwd: 'src/assets/fonts',
					src: ['**/*.{eot,woff,ttf,svg}'],
					dest: 'dist/assets/fonts'
				}]
			}
		},


		// Imagemin task - Extra compression and minification for images and svg.
		// Docs: https://github.com/gruntjs/grunt-contrib-imagemin
		imagemin: {
			assets: {
				options: {
					optimizationLevel: 3,
					svgoPlugins: [{ removeViewBox: false }, { removeUselessStrokeAndFill: false }]
				},
				files: [{
					expand: true,
					cwd: 'dist/assets/img',
					src: ['*.{png,jpg,gif,svg}'],
					dest: 'dist/assets/img'
				}]
			}
		},

    grunticon: {
      app: {
        files: [{
          expand: true,
          cwd: 'src/assets/img',
          src: ['icons/*.svg', 'icons/*.png'],
          dest: "dist/assets/img"
        }]
      }
    },


		// Uglify task - Minify files with UglifyJS.
		// Docs: https://github.com/gruntjs/grunt-contrib-uglify
		uglify: {
			app: {
				options: {
					compress: {},
					mangle: true,
					beautify: false,
					sourceMap: true,
					preserveComments: false,
					drop_console: true,
					banner: '/*! <%= config.projectname %> - application - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
				},
				files: {
					'dist/js/app.min.js': srcJsFiles
				}
			},
			libs: {
				options: {
					compress: {},
					mangle: true,
					beautify: false,
					sourceMap: true,
					preserveComments: false,
					drop_console: true,
					banner: '/*! <%= config.projectname %> - libraries - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
				},
				files: {
					'dist/libs/libs.min.js': ['src/libs/libs.js', ['src/libs/*.js', 'src/libs/libs.js']]
				}
			}
		},


		// Autoprefixer task - Add vendor-prefixed CSS using Can I Use database.
		// Docs: https://github.com/nDmitry/grunt-autoprefixer
		autoprefixer: {
			app: {
				options: {
					browsers: ['last 2 versions', 'ie 8', 'ie 9', 'Firefox >= 25'],
					map: true,
					cascade: false
				},
				src: 'dist/css/app.css',
				dest: 'dist/css/app.css'
			}
		}

	});

	// helper (sub)tasks
	grunt.registerTask('update-libs', ['bower_concat', 'uglify:libs']);
	grunt.registerTask('update-fonts', ['copy:fonts']);
	grunt.registerTask('update-images', ['newer:imagemin']);
	grunt.registerTask('update-css', ['newer:sass:app', 'newer:autoprefixer:app']);
	grunt.registerTask('update-javascript', ['jshint', 'uglify:app']);


	// main tasks
	grunt.registerTask('init', [
		'update-libs',
		'update-fonts',
		'update-images',
		'update-css',
		'update-javascript'
	]);

	grunt.registerTask('assets', [
		'update-fonts',
		'update-images'
	]);

	grunt.registerTask('default', [
		'update-fonts',
		'update-images',
		'update-css',
		'update-javascript'
	]);

};